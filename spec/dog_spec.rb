require 'spec_helper'
require_relative '../dog'

describe Dog do
  describe '#eat' do
    it 'eats' do
      expect(Dog.new.eat).to eq('eatssss')
    end

    it 'eats again' do
      expect(Dog.new.eat).to eq('eatssss again')
    end

    #200.times do |i|
      #it "fails #{i}" do
        #expect(Dog.new.eat).to eq("eat fail #{i}")
      #end
    #end
  end
end
