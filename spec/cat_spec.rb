require 'spec_helper'
require_relative '../cat'

describe Cat do
  let(:cat) { described_class.new }

  describe '#eat' do
    it 'eats' do
      expect(cat.eat).to eq('eatssss')
    end
  end

  describe '#growl' do
    it 'growls' do
      expect(cat.eat).to eq('growl')
    end
  end
end
