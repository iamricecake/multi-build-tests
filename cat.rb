class Cat
  def eat
    'eat'
  end

  def growl
    'meow'
  end

  def run
    'run'
  end
end
